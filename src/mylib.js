/** Basic arithmetic operations */
const mylib = {
    /** Add two numbers together. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    /** Substract number b from number a */
    substract: (a, b) => {
        return a - b;
    },
    /** Divide divident by divisor */
    divide: (divident, divisor) => {
        if (divisor == 0) {
            throw new Error("Division by zero.");
        }
        return Math.round((divident / divisor) * 10) / 10;
    },
    /** Multiply numbers */
    multiply: function (a, b) {
        return a * b;
    },
};

module.exports = mylib;