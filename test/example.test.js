const expect = require("chai").expect;
const { AssertionError } = require("chai");
const mylib = require("../src/mylib");

describe("Our first unit tests", () => {
    before(() => {
        // Initialization 
        // Create objects... etc...
        console.log("Initializing completed.");
    });
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1, 2)).equal(3, "1 + 2 != 3 apparently... e_e");
    });
    it("Can substract 1 and 2", () => {
        expect(mylib.substract(1, 2)).equal(-1, "1 - 2 != -1 apparently... e_e");
    });
    it("Can multiply 3 and 7", () => {
        expect(mylib.multiply(3, 7)).equal(21, "3 * 7 != 21 apparently... e_e");
    });
    it(("Division by zero throws error"), () => {
        expect(function() { mylib.divide(3, 0)}).to.throw("Division by zero.");
    });
    it(("Rounds to one decimal properly"), () => {
        expect(mylib.divide(8, 3)).equal(2.7, "Division rounded incorrectly.");
    });
    after(() => {
        // Cleanup
        // For example shut down the Express server...
        console.log("Testing completed!");
    });
});